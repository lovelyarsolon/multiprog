
//User class
function User (id, res, time) {
  this.id = id
  this.res = res
  this.time = time
  this.printUser = function () {
    console.log(`I am ${id} my time is: ${time}`)
    return;
  }
}

function Resource (id, users, status, timeAvail) {
  this.id = id
  this.users = users
  this.status = status
  this.timeAvail = timeAvail
  this.printResourec = function () {
    console.log(`I am resource ${id} my status is: ${status} and my time of availability is: ${timeAvail}`);
    return;
  }
}


new Vue({
  el: "#app",
//   delimiters: ["${", "}"],
  data: {
    resIds: [],
    userIds: [],
    userList: [],
    resourceList: [],
  },
  mounted: function () {
    // this.checkCookie(); // Check if private room and user already exists
  },
  methods: {

    randomize: function () {
      rand = Math.floor(Math.random() * (10 - 1 + 1)) + 1;
      return rand;
    },

    setResourceIds: function (num) {
      // len = this.resIds.length;
      // console.log("initial length: " + len);
      console.log("length should be: " + num);
      while (this.resIds.length != num) {
        res = this.randomize();

        // b = this.resIds.includes(res) != true ? "I am not a duplicate" : "I am a duplicate";
        // console.log(res + ":  " + b);
        if (this.resIds.includes(res) != true) {
          this.resIds.push(res);
          // len = this.resIds.length;
        }
      }
      console.log("length after is: " + this.resIds.length);
      return;
    },

    setUserIds: function (num) {
      // len = this.userIds.length;
      // console.log("initial length of users list: " + len);
      while (this.userIds.length != num) {
        user = this.randomize();

        if (this.userIds.includes(user) != true) {
          this.userIds.push(user);
          // len = this.userIds.length;
        }
      }
      return;
    },

    setUserResIds: function (num) {
      userResIds = [];

      len = 0;
      while (len != num) {
        res = this.randomize();
        
        if (userResIds.includes(res) != true) {
          userResIds.push(res);
          len++;
        }
      }
      return userResIds;
    },

    setUsers: function (num) {
      for (let i = 0; i < num; i++) {
        userId = this.userIds[i]
        userRes = this.randomize(); //set the number of resources for the user
        userResIds = this.setUserResIds(userRes); //set the resource ids for the user
        userTime = this.randomize(); //set time of the user
        // console.log("my resources: " + userResIds);
        user = new User(userId, userResIds, userTime);
        // console.log(user.printUser());
        this.userList.push(user);
      }
      return;
    },

    getUserIds: function (id) {
      resUserIds = []
      len = this.userList.length;
      for (let i = 0; i < len; i++) {
        user = this.userList[i];
        userResource = user.res;
        if (userResource.includes(id)) {
          resUserIds.push(user);
        }
      }
      return resUserIds;
    },

    setResources: function () {
      for (let i = 0; i < this.resIds.length; i++) {
        userss = this.getUserIds(this.resIds[i]);
        res = new Resource(this.resIds[i], userss, "idle", 0);
        this.resourceList.push(res);
      }
      return;
    },

    start: function () {
      r = this.randomize(); //set the number of resources to be used
      console.log("length from randomized: " + r);
      this.setResourceIds(r); //set the ids of the resources
      users = this.randomize();
      this.setUserIds(users);

      console.log("randomized length: " + r);
      console.log("res length: " + this.resIds.length);
      console.log("users length: " + users);

      // sconsole.log("not sorted resources: " + this.resIds + " length: " + this.resIds.length);
      // console.log("not sorted users: " + this.userIds + " length: " + this.userIds.length);

      this.resIds.sort(function(a, b){return a-b});
      this.userIds.sort(function(a, b){return a-b});
      this.setUsers(users); //set users with their ids, resources, and time
      this.setResources();
      
      for (let i = 0; i < this.resourceList.length; i++) {
        console.log("id is: " + this.resourceList[i].id);
        console.log(this.resourceList[i].users);
      }

    },
  }
});

